﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Entity
{
    public class Klient
    {
        [BsonId]
        public Guid Id { get; set; }
        public String Imie { get; set; }
        public String Nazwisko { get; set; }

        public Klient(String Imie, String Nazwisko)
        {
            this.Imie = Imie;
            this.Nazwisko = Nazwisko;
        }
        public Klient() { }
    }
}
