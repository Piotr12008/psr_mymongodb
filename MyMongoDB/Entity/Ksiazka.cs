﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Entity
{
    public class Ksiazka
    {
        [BsonId]
        public Guid Id { get; set; }
        public string Tytul { get; set; }
        public String Autorzy { get; set; }
        public string ISBN { get; set; }
        public bool Dostepna { get; set; }

        public Ksiazka(string Tytul, string Autorzy, string ISBN)
        {
            this.Tytul = Tytul;
            this.Autorzy = Autorzy;
            this.ISBN = ISBN;
            this.Dostepna = true;
        }
        public Ksiazka()
        { }
    }
}
