﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Entity
{
    public class Wypozyczenie
    {
        [BsonId]
        public Guid Id { get; set; }
        public String Ksiazka { get; set; }
        public String Klient { get; set; }
        public DateTime DataWyp { get; set; }
        public DateTime DataOdda { get; set; }
        public bool Oddane { get; set; }

        public Wypozyczenie(String Ksiazka, String Klient, DateTime DataWyp)
        {
            this.Ksiazka = Ksiazka;
            this.Klient = Klient;
            this.DataWyp = DataWyp;
            this.Oddane = false;
        }
        public Wypozyczenie()
        { }
    }
}
