﻿using MongoDB.Bson;
using MongoDB.Driver;
using MyMongoDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Repository
{
    public class KlientRepo
    {
        public IMongoDatabase DB;
        public KlientRepo(IMongoDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Klient klient)
        {
            try
            {
                var collection = DB.GetCollection<Klient>("Klient");
                collection.InsertOne(klient);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Klient> FindAll()
        {
            var collection = DB.GetCollection<Klient>("Klient");
            return collection.Find(new BsonDocument()).ToList();
        }
        public Klient FindOne(String id)
        {
            var collection = DB.GetCollection<Klient>("Klient");
            FilterDefinition<Klient> filter = Builders<Klient>.Filter.Eq("Id", id);
            Klient client = new Klient();
            try
            {
                client = collection.Find(filter).First();
            }catch(Exception)
            {
                return null;
            }
            return client;
        }
        public bool Update(String id, Klient klient)
        {
            try
            {
                var collection = DB.GetCollection<Klient>("Klient");
                FilterDefinition<Klient> filter = Builders<Klient>.Filter.Eq("Id", id);
                UpdateDefinition<Klient> update;
                if (!klient.Imie.Equals(""))
                {
                    update = Builders<Klient>.Update.Set("Imie", klient.Imie);
                    collection.UpdateOne(filter, update);
                }
                if (!klient.Nazwisko.Equals(""))
                {
                    update = Builders<Klient>.Update.Set("Nazwisko", klient.Nazwisko);
                    collection.UpdateOne(filter, update);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Remove(String id)
        {
            try
            {
                var collection = DB.GetCollection<Klient>("Klient");
                FilterDefinition<Klient> filter = Builders<Klient>.Filter.Eq("Id", id);
                collection.DeleteOne(filter);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
