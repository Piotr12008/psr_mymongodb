﻿using MongoDB.Bson;
using MongoDB.Driver;
using MyMongoDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Repository
{
    public class KsiazkaRepo
    {
        public IMongoDatabase DB;
        public KsiazkaRepo(IMongoDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Ksiazka ksiazka)
        {
            try
            {
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                collection.InsertOne(ksiazka);
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        }
        public List<Ksiazka> FindAll()
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            return collection.Find(new BsonDocument()).ToList();
        }
        public Ksiazka FindOne(String id)
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            FilterDefinition<Ksiazka> filter = Builders<Ksiazka>.Filter.Eq("Id", id);
            Ksiazka ksiazka = new Ksiazka();
            try
            {
                ksiazka = collection.Find(filter).First();
            }catch(Exception)
            {
                return null;
            }
            return ksiazka;
        }
        public List<Ksiazka> FindAllAvailable()
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            FilterDefinition<Ksiazka> filter2 = Builders<Ksiazka>.Filter.Eq("Dostepna", true);
            List<Ksiazka> ksiazki = new List<Ksiazka>();
            try
            {
                ksiazki = collection.Find(filter2).ToList();
            }catch(Exception)
            {
                return ksiazki;
            }
            return ksiazki;
        }
        public Ksiazka FindOne(Ksiazka ksiazka)
        {
            var collection = DB.GetCollection<Ksiazka>("Ksiazka");
            FilterDefinition<Ksiazka> filter = Builders<Ksiazka>.Filter.Eq("Tytul", ksiazka.Tytul);
            Ksiazka k = new Ksiazka();
            try
            {
                k = collection.Find(filter).First();
            }
            catch (Exception)
            {
                return null;
            }
            return k;
        }
        public bool Update(String id, Ksiazka ksiazka)
        {
            try
            {
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                FilterDefinition<Ksiazka> filter = Builders<Ksiazka>.Filter.Eq("Id", id);
                UpdateDefinition<Ksiazka> update;
                if (!ksiazka.Tytul.Equals(""))
                {
                    update = Builders<Ksiazka>.Update.Set("Tytul", ksiazka.Tytul);
                    collection.UpdateOne(filter, update);
                }
                if (!ksiazka.Autorzy.Equals(""))
                {
                    update = Builders<Ksiazka>.Update.Set("Autorzy", ksiazka.Autorzy);
                    collection.UpdateOne(filter, update);
                }
                if (!ksiazka.ISBN.Equals(""))
                {
                    update = Builders<Ksiazka>.Update.Set("ISBN", ksiazka.ISBN);
                    collection.UpdateOne(filter, update);
                }
            }
            catch(Exception)
            {
                return false;
            }
            return true;           
        }
        public bool Remove(String id)
        {
            try
            {
                var collection = DB.GetCollection<Ksiazka>("Ksiazka");
                FilterDefinition<Ksiazka> filter = Builders<Ksiazka>.Filter.Eq("Id", id);
                collection.DeleteOne(filter);
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        }
    }
}
