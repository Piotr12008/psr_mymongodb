﻿using MongoDB.Bson;
using MongoDB.Driver;
using MyMongoDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyMongoDB.Repository
{
    public class WypozyczenieRepo
    {
        public IMongoDatabase DB;
        public WypozyczenieRepo(IMongoDatabase Database)
        {
            this.DB = Database;
        }
        public bool Save(Wypozyczenie wypozyczenie)
        {
            try
            {
                var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
                collection.InsertOne(wypozyczenie);
                KsiazkaRepo ksiazkaRepo = new KsiazkaRepo(DB);

                var collection2 = DB.GetCollection<Ksiazka>("Ksiazka");
                FilterDefinition<Ksiazka> filter = Builders<Ksiazka>.Filter.Eq("Id", wypozyczenie.Ksiazka);
                UpdateDefinition<Ksiazka> update;
                update = Builders<Ksiazka>.Update.Set("Dostepna", false);
                collection2.UpdateOne(filter, update);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Zwrot(Wypozyczenie wypozyczenie)
        {
            try
            {
                var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
                FilterDefinition<Wypozyczenie> filter = Builders<Wypozyczenie>.Filter.Eq("Id", wypozyczenie.Id);
                UpdateDefinition<Wypozyczenie> update;
                update = Builders<Wypozyczenie>.Update.Set("DataOdda", DateTime.Now);
                collection.UpdateOne(filter, update);
                update = Builders<Wypozyczenie>.Update.Set("Oddane", true);
                collection.UpdateOne(filter, update);

                KsiazkaRepo ksiazkaRepo = new KsiazkaRepo(DB);
                var collection2 = DB.GetCollection<Ksiazka>("Ksiazka");
                FilterDefinition<Ksiazka> filter2 = Builders<Ksiazka>.Filter.Eq("Id", wypozyczenie.Ksiazka);
                UpdateDefinition<Ksiazka> update2;
                update2 = Builders<Ksiazka>.Update.Set("Dostepna", true);
                collection2.UpdateOne(filter2, update2);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public List<Wypozyczenie> FindAll(String id)
        {
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            FilterDefinition<Wypozyczenie> filter = Builders<Wypozyczenie>.Filter.Eq("Klient", id);
            return collection.Find(filter).ToList().OrderBy(s => s.Oddane).ToList();
        }
        public List<Wypozyczenie> FindAllNow(String id)
        {
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            FilterDefinition<Wypozyczenie> filter1 = Builders<Wypozyczenie>.Filter.Eq("Klient", id);
            FilterDefinition<Wypozyczenie> filter2 = Builders<Wypozyczenie>.Filter.Eq("Oddane", false);
            FilterDefinition<Wypozyczenie> filter = Builders<Wypozyczenie>.Filter.And(filter1, filter2);
            return collection.Find(filter).ToList();
        }
        public Wypozyczenie FindOne(String id)
        {
            var collection = DB.GetCollection<Wypozyczenie>("Wypozyczenie");
            FilterDefinition<Wypozyczenie> filter = Builders<Wypozyczenie>.Filter.Eq("Id", id);
            Wypozyczenie wypozyczenie = new Wypozyczenie();
            try
            {
                wypozyczenie = collection.Find(filter).First();
            }
            catch (Exception)
            {
                return null;
            }
            return wypozyczenie;
        }
    }
}
