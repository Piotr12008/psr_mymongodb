﻿using MongoDB.Bson;
using MongoDB.Driver;
using MyMongoDB.Entity;
using MyMongoDB.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyMongoDB
{
    class Program
    {
        public static void MenuGlowne()
        {
            Console.WriteLine("MENU GŁÓWNE");
            Console.WriteLine("1. Ksiązki");
            Console.WriteLine("2. Klienci");
            Console.WriteLine("3. Wypożyczenia");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuKsiazka()
        {
            Console.WriteLine("MENU Ksiażka");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Edytuj");
            Console.WriteLine("4. Usuń");
            Console.WriteLine("5. Szukaj");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuKlient()
        {
            Console.WriteLine("MENU Klient");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Edytuj");
            Console.WriteLine("4. Usuń");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuWypozyczenia()
        {
            Console.WriteLine("MENU Wypożyczenia");
            Console.WriteLine("1. Wypożyczenie");
            Console.WriteLine("2. Zwrot");
            Console.WriteLine("3. Wypożyczenia użytkowników");
            Console.WriteLine("4. Dostępne książki");
            Console.WriteLine("q. Wyjście");
        }
        static void Main(string[] args)
        {
            ConsoleKey key = new ConsoleKey();
            Console.WriteLine("Tworzę bazę !");
            var client = new MongoClient();
            var database = client.GetDatabase("MyMongoDB_PSR");
            KsiazkaRepo ksiazkaRepo = new KsiazkaRepo(database);
            KlientRepo klientRepo = new KlientRepo(database);
            WypozyczenieRepo wypozyczenieRepo = new WypozyczenieRepo(database);

            Console.WriteLine("Utworzyłem baze MyMongoDB_PSR!");
            Console.WriteLine("Tworzę/sprawdzam wstępne dane");
            Ksiazka k = new Ksiazka("Władca pierścini", "J.R.R. Tolkien", "1212123456");
            Ksiazka k2 = new Ksiazka("Hobbit", "J.R.R. Tolkien", "1212123477");
            Ksiazka k3 = new Ksiazka("Pan Tadeusz", "Adam Mickiewicz", "1212123897");

            Klient kl = new Klient("Michał", "Nowakowski");
            Klient kl2 = new Klient("Karol", "Kowalski");

            var collection = database.GetCollection<Ksiazka>("Ksiazka");
            if (collection.Find(new BsonDocument()).ToList().Count == 0)
                collection.InsertMany(new List<Ksiazka> { k, k2, k3});

            var collection2 = database.GetCollection<Klient>("Klient");
            if (collection2.Find(new BsonDocument()).ToList().Count == 0)
                collection2.InsertMany(new List<Klient> { kl, kl2});


            while (true)
            {
                MenuGlowne();
                key = Console.ReadKey().Key;
                if (key.Equals(ConsoleKey.D1))
                {
                    while (true)
                    {
                        MenuKsiazka();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj tyyuł: ");
                            String tytul = Console.ReadLine();
                            Console.WriteLine("Podaj Autorów: ");
                            String autorzy = Console.ReadLine();
                            Console.WriteLine("Podaj ISBN: ");
                            String isbn = Console.ReadLine();
                            Ksiazka ksiazka = new Ksiazka(tytul, autorzy, isbn);
                            if (ksiazkaRepo.Save(ksiazka) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Ksiazka> ksiazki = ksiazkaRepo.FindAll();
                            foreach (Ksiazka ksiazka in ksiazki)
                            {
                                string dostepna = "";
                                if (ksiazka.Dostepna == true)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(ksiazka.Id + " --- " + ksiazka.Tytul + " - " + ksiazka.Autorzy + " - " + ksiazka.ISBN + " (" + dostepna + ")");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID książki do edycji: ");
                            List<Ksiazka> ksiazki = ksiazkaRepo.FindAll();
                            foreach (Ksiazka ksiazka in ksiazki)
                            {
                                string dostepna = "";
                                if (ksiazka.Dostepna == true)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(ksiazka.Id + " --- " + ksiazka.Tytul + " - " + ksiazka.Autorzy + " - " + ksiazka.ISBN + " (" + dostepna + ")");
                            }
                            String id = Console.ReadLine();
                            if (ksiazkaRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj tylko dane które chcesz zmienić: ");
                                Console.WriteLine("Podaj tyyuł: ");
                                String tytul = Console.ReadLine();
                                Console.WriteLine("Podaj Autorów: ");
                                String autorzy = Console.ReadLine();
                                Console.WriteLine("Podaj ISBN: ");
                                String isbn = Console.ReadLine();
                                Ksiazka ksiazka = new Ksiazka(tytul, autorzy, isbn);
                                if (ksiazkaRepo.Update(id, ksiazka) == true)
                                    Console.WriteLine("Zapisano !");
                                else
                                    Console.WriteLine("Błąd zapisu");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj ID książki do usuniecia: ");
                            List<Ksiazka> ksiazki = ksiazkaRepo.FindAll();
                            foreach (Ksiazka ksiazka in ksiazki)
                            {
                                string dostepna = "";
                                if (ksiazka.Dostepna == true)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(ksiazka.Id + " --- " + ksiazka.Tytul + " - " + ksiazka.Autorzy + " - " + ksiazka.ISBN + " (" + dostepna + ")");
                            }
                            String id = Console.ReadLine();
                            if (ksiazkaRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (ksiazkaRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D5))
                        {
                            Console.WriteLine("Podaj tyyuł: ");
                            String tytul = Console.ReadLine();
                            Ksiazka ks = new Ksiazka();
                            Ksiazka ksiazka = new Ksiazka();
                            ks.Tytul = tytul;
                            ksiazka = ksiazkaRepo.FindOne(ks);
                            if (ksiazka != null)
                            {
                                string dostepna = "";
                                if (ksiazka.Dostepna == true)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(ksiazka.Id + " --- " + ksiazka.Tytul + " - " + ksiazka.Autorzy + " - " + ksiazka.ISBN + " (" + dostepna + ")");
                            }
                            else
                                Console.WriteLine("Nie znaleziono książki.");
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D2))
                {
                    while (true)
                    {
                        MenuKlient();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj Imie: ");
                            String imie = Console.ReadLine();
                            Console.WriteLine("Podaj Nazwisko: ");
                            String nazwisko = Console.ReadLine();
                            Klient klient = new Klient(imie, nazwisko);
                            if (klientRepo.Save(klient) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.Id + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID książki do edycji: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.Id + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            if (klientRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj tylko dane które chcesz zmienić: ");
                                Console.WriteLine("Podaj imie: ");
                                String imie = Console.ReadLine();
                                Console.WriteLine("Podaj nazwisko: ");
                                String nazwisko = Console.ReadLine();
                                Klient klient = new Klient(imie, nazwisko);
                                if (klientRepo.Update(id, klient) == true)
                                    Console.WriteLine("Zapisano !");
                                else
                                    Console.WriteLine("Błąd zapisu");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj ID książki do usuniecia: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.Id + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            if (klientRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (klientRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D3))
                {
                    while (true)
                    {
                        MenuWypozyczenia();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj ID klienta który wypożycza: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.Id + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj po enterze id książek do wypożyczenia (wpisz q aby zakończyć)");
                                List<Ksiazka> ksiazki = ksiazkaRepo.FindAllAvailable();
                                if (ksiazki.Count == 0)
                                {
                                    Console.WriteLine("Brak książek");
                                    continue;
                                }
                                foreach (Ksiazka ks in ksiazki)
                                {
                                    string dostepna = "";
                                    if (ks.Dostepna == true)
                                        dostepna = "dostępna";
                                    else
                                        dostepna = "niedostępna";
                                    Console.WriteLine(ks.Id + " --- " + ks.Tytul + " - " + ks.Autorzy + " - " + ks.ISBN + " (" + dostepna + ")");
                                }
                                String ksiazkaId = "";
                                while (!(ksiazkaId = Console.ReadLine()).Equals("q"))
                                {
                                    //String ksiazkaId = Console.ReadLine();
                                    Ksiazka ksiazka = ksiazkaRepo.FindOne(ksiazkaId);
                                    if (ksiazka == null)
                                    {
                                        Console.WriteLine("Błędny numer ID");
                                        continue;
                                    }
                                    Wypozyczenie wypozyczenie = new Wypozyczenie(ksiazkaId, klientId, DateTime.Now);
                                    if (wypozyczenieRepo.Save(wypozyczenie) == true)
                                        Console.WriteLine("Wypozyczono !");
                                    else
                                        Console.WriteLine("Błąd wypożyczenia");
                                }
                            }
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            Console.WriteLine("Podaj ID klienta: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.Id + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                List<Wypozyczenie> wypozyczenia = wypozyczenieRepo.FindAllNow(klientId);
                                Console.WriteLine("Wypożyczenia " + klient.Imie + " - " + klient.Nazwisko + " :");
                                foreach (Wypozyczenie wypozyczenie in wypozyczenia)
                                {
                                    Ksiazka wypozyczona = ksiazkaRepo.FindOne(wypozyczenie.Ksiazka);
                                    if (wypozyczenie.Oddane == false)
                                    {
                                        Console.WriteLine(wypozyczenie.Id + " --- " + wypozyczenie.DataWyp + " - " + wypozyczona.Tytul + " - " + wypozyczona.Autorzy + " - " + wypozyczona.ISBN);
                                    }
                                }
                                String wypozyczenieId = "";
                                while (!(wypozyczenieId = Console.ReadLine()).Equals("q"))
                                {
                                    //String ksiazkaId = Console.ReadLine();
                                    Wypozyczenie wypozyczenie = wypozyczenieRepo.FindOne(wypozyczenieId);
                                    if (wypozyczenie == null)
                                    {
                                        Console.WriteLine("Błędny numer ID");
                                        continue;
                                    }
                                    if (wypozyczenieRepo.Zwrot(wypozyczenie) == true)
                                        Console.WriteLine("Zwrócono !");
                                    else
                                        Console.WriteLine("Błąd zwracania książkiq");
                                }
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID klienta: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.Id + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                List<Wypozyczenie> wypozyczenia = wypozyczenieRepo.FindAll(klientId);
                                Console.WriteLine("Wypożyczenia " + klient.Imie + " - " + klient.Nazwisko + " :");
                                foreach (Wypozyczenie wypozyczenie in wypozyczenia)
                                {
                                    Ksiazka wypozyczona = ksiazkaRepo.FindOne(wypozyczenie.Ksiazka);
                                    if (wypozyczenie.Oddane == false)
                                    {
                                        Console.WriteLine("[Aktualne] --- " + wypozyczenie.DataWyp + " - " + wypozyczona.Tytul + " - " + wypozyczona.Autorzy + " - " + wypozyczona.ISBN);
                                    }
                                    else
                                    {
                                        Console.WriteLine(wypozyczenie.DataWyp + " do " + wypozyczenie.DataOdda + " - " + wypozyczona.Tytul + " - " + wypozyczona.Autorzy + " - " + wypozyczona.ISBN);
                                    }
                                }
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            List<Ksiazka> ksiazki = ksiazkaRepo.FindAllAvailable();
                            if (ksiazki.Count == 0)
                            {
                                Console.WriteLine("Brak książek");
                            }
                            foreach (Ksiazka ks in ksiazki)
                            {
                                Console.WriteLine(ks.Id + " --- " + ks.Tytul + " - " + ks.Autorzy + " - " + ks.ISBN);
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.Q))
                {
                    break;
                }
            }
        }
    }
}
